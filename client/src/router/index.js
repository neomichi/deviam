import Vue from "vue";
import VueRouter from "vue-router";
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
      meta: { role: 0,title:'Главная',showInMenu:true },
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      layout: false,
      meta: { role: 0,title:'Резюме',showInMenu:true  },
      path: "/resume",
      name: "resume",
      component: () => import("../views/Resume.vue")
    },
    {
      meta: { role: 1,title:'Лк',showInMenu:false },
      path: "/user/:userUrl",
      name: "user",
      component: () => import("../views/User.vue")
    },
    {
      layout: false,
      meta: { layout: false, role: 0,title:'Вход',showInMenu:true  },
      path: "/login",
      name: "login",
      component: () => import("../views/Auth.vue")
    },
    {
      layout: false,
      meta: { layout: false, role: 0,title:'Регистрация',showInMenu:false },
      path: "/register",
      name: "register",
      component: () => import("../views/Auth.vue")
      //beforeEnter:routeBeforeEnter
    },
  ]
  const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
  });

  export default router;
