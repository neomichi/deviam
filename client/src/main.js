
import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
Vue.config.productionTip = false
import 'materialize-css/dist/css/materialize.css'
import 'materialize-css/dist/js/materialize'
import './assets/css/index.css'
import  './assets/css/flex.css'
import  Helper from './assets/js/helperPlugin'

Vue.use(Helper)
/* eslint-disable no-new */
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");








