import Vue from "vue"
import Vuex from "vuex"
import HelperPlugin from '../assets/js/helperPlugin'

Vue.use(Vuex)
Vue.use(HelperPlugin)

const helperPlugin = new Vue()
console.log(helperPlugin.$helper.test)
export default new Vuex.Store({
    
    state: {
        token: helperPlugin.$helper.getCookie(),
        user: { id: 0 },
        showLoad: false,
      },
      getters: {
        tokenIsEmpty: (state) => {
          return !state.token || 0 === state.token.length;
        },
        userIsEmpty: (state) => {
          const user = state.user;
          return !user || !user.id || user.id == 0 || 0 === user.id.length;
        },
        isAuth: (state) => {
          return (
            state.token &&
            state.token.length > 0 &&
            Object.keys(state.user).indexOf("email") >= 0
          );
        },
      },
      mutations: {
        setLoader(state, obj) {
          state.showLoad = obj;
        },
        setToken(state, obj) {
          helperPlugin.$helper.setCookie(obj, 5);
          state.token = obj;
        },
        setUser(state, obj) {
          state.user = obj;
        },
        logout(state) {
            helperPlugin.$helper.deleteCookie();
          state.user = new Object();
        },
      },
      actions: {
        async updateToken({ commit }, authData) {
          try {
            const token = await axios({
              method: "post",
              url: "http://localhost:5000/api/token/",
              data: authData,
            });
            const result = await token.data.access_token;
    
            commit("setToken", result);
    
            return true;
          } catch (error) {
            if (error.response == undefined) {
              Toast("ошибка, проблемы с сетью", 3);
            }
    
            console.log(`error ${error.response}`);
    
            return false;
          }
        },
        async updateUser({ commit }, obj) {
          try {
            const user = await axios.get("http://localhost:5000/api/account/", {
              headers: { Authorization: `Bearer ${obj.value}` },
            });
            commit("setUser", user.data);
            if (obj.showToast) {
              Toast("Вы вошли в аккаунт ", 4);
            }
            return true;
          } catch(e) {
            if (obj.showToast) {
              Toast("Ошибка, войдите в аккаунт ", 3);
            }
            return false;
          }
        },
        logout({ commit }, showToast) {
          console.log(`logut ${showToast}`);
          if (showToast) {
            Toast("Вы вышли из аккаунта ", 1);
          }
          commit("logout");
        },
        async SendFileToBack(aa, query) {
          try {
            const resp = await axios.post(query.url, {
              data: query.data,
            });
            return resp;
          } catch(e) {
            console.log("SendOurHelperData error");
          }
        },
        async LoaderEvent({ commit }, showOrhide) {
          commit("setLoader", showOrhide);
        },
        
      },
})