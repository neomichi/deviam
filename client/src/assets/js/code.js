
export const Toast = (text, type = 1) => {
  let cName = "toast-info";
  switch (type) {
    case 1:
      cName = "toast-info";
      break;
    case 2:
      cName = "toast-warning";
      break;
    case 3:
      cName = "toast-error";
      break;
    case 4:
      cName = "toast-success";
      break;
  }
  // console.log(`toast ${text} ${cName}`);
  window.M.toast({ html: text, classes: cName, displayLength: 2500 });
};
export const square = (x, y) => x * y;
export const getKeys = (obj) => Object.keys(obj);

const cookieName = "token";

export const setCookie = (value, minutes = 15) => {
  let expires = "";
  if (minutes) {
    var date = new Date();
    date.setTime(date.getTime() + minutes * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = cookieName + "=" + (value || "") + expires + "; path=/";
};

export const getCookie = () => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${cookieName}=`);
  if (parts.length === 2)
    return parts
      .pop()
      .split(";")
      .shift();
};

export const deleteCookie = () => {
  document.cookie = cookieName + "=; Max-Age=-99999999;";
};

export const dispatchData = (value, toastIsHidden = false) => {
  return { value, toastIsHidden };
};

export const beforeEach = (router,store,to, from, next) => {

      let currentRole=0;

      let role=store.getters.tokenIsEmpty?'':store.state.user.roles;
      if (role=='admin') currentRole=2;
      if (role=='manager') currentRole=1;

     
    
    // const routes = router.options.routes
    //   .filter((route) => route.meta.role > 0)
    //   .map((route) => {
    //     return {
    //       name: route.name,
    //       path: route.path,
    //       title: route.meta.title,
    //       role: route.meta.role,
    //     };
    //   });

    if (currentRole < to.meta.role) {
      next({ name: "login" });
      Toast("У вас нет прав ", 2);
    }

    // console.log(routes);
    // console.log(`beforeEach to`);
    // console.log(to);
    // console.log(`beforeEach from`);
    // console.log(from);
    // console.log(`beforeEach next  ${next}`);
    // console.log(`tokenIsEmpty ${store.getters.tokenIsEmpty}`);
    // console.log(`userIsEmpty ${store.getters.userIsEmpty}`);
    // console.log(`isAuth ${store.getters.isAuth}`);

    next();
  }

export const TokenState = {};



export const objFilter =(data,allowedArray)=>  Object.assign({}, ...allowedArray.map(key=> ({[key]:data[key]})));

export const mapFromSource=(object, source)=> {
  Object.keys(object).forEach(key=> object[key] = source[key]);
}
export const ObjectfilteredByValue=(obj,val) => Object.fromEntries(Object.entries(obj).filter(([key, value]) => key === val))

export const test='work'