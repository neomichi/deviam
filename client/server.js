const fastify = require('fastify')({
    logger: true
  })

  fastify.get('/', (request, reply) => {
    reply.sendFile('./test-3/dist/index.html')
  })
   
  // Run the server!
  fastify.listen(8000, (err, address) => {
    if (err) throw err
    fastify.log.info(`server listening on ${address}`)
  })